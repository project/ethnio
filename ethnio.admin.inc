<?php

/**
 * @file
 * Administrative page callbacks for the ethnio module.
 */

/**
 * Ethnio settings form.
 */
function ethnio_admin_settings_form($form_state) {
  $form = array();

  $form['ethnio_id'] = array(
    '#title' => t('Ethnio ID'),
    '#type' => 'textfield',
    '#default_value' => variable_get('ethnio_id'),
    '#size' => 15,
    '#maxlength' => 20,
    '#required' => TRUE,
    '#description' => t('Your Ethnio ID can be found in your tracking code <code>src=\"//ethn.io/<b>12345</b>.js</code> where <code><b>12345</b></code> is your Ethnio ID'),
  );

  $options = array(
    '1' => '100% of visitors',
    '2' => '50% of visitors',
    '3' => '33% of visitors',
    '5' => '20% of visitors',
    '10' => '10% of visitors',
    '100' => '1% of visitors',
    '1000' => '0.1% of visitors',
  );

  $form['ethnio_display_probability'] = array(
    '#type' => 'select',
    '#title' => t('Ethnio display probability.'),
    '#options' => $options,
    '#default_value' => variable_get('ethnio_display_probability', 10),
    '#description' => t('This controls what fraction of site visitors will see the Ethnio popup.'),
  );

  $form['tracking'] = array(
    '#type' => 'vertical_tabs',
  );

  $visibility = variable_get('ethnio_visibility_pages', 0);
  $pages = variable_get('ethnio_pages', ETHNIO_PAGES);

  $form['tracking']['page_track'] = array(
    '#type' => 'fieldset',
    '#title' => t('Pages'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $options = array(
    t('Every page except the listed pages'),
    t('The listed pages only'),
  );
  $description_args = array(
    '%blog' => 'blog',
    '%blog-wildcard' => 'blog/*',
    '%front' => '<front>',
  );
  $description = t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", $description_args);

  $form['tracking']['page_track']['ethnio_visibility_pages'] = array(
    '#type' => 'radios',
    '#title' => t('Add tracking to specific pages'),
    '#options' => $options,
    '#default_value' => $visibility,
  );
  $form['tracking']['page_track']['ethnio_pages'] = array(
    '#type' => 'textarea',
    '#title' => t('Pages'),
    '#title_display' => 'invisible',
    '#default_value' => $pages,
    '#description' => $description,
    '#rows' => 10,
  );

  // Render the role overview.
  $form['tracking']['role_track'] = array(
    '#type' => 'fieldset',
    '#title' => t('Roles'),
  );

  $form['tracking']['role_track']['ethnio_visibility_roles'] = array(
    '#type' => 'radios',
    '#title' => t('Add tracking for specific roles'),
    '#options' => array(
      t('Add to the selected roles only'),
      t('Add to every role except the selected ones'),
    ),
    '#default_value' => variable_get('ethnio_visibility_roles', 0),
  );

  $role_options = array_map('check_plain', user_roles());
  $form['tracking']['role_track']['ethnio_roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Roles'),
    '#default_value' => variable_get('ethnio_roles', array()),
    '#options' => $role_options,
    '#description' => t('If none of the roles are selected, all users will be tracked. If a user has any of the roles checked, that user will be tracked (or excluded, depending on the setting above).'),
  );

  return system_settings_form($form);
}
