# Ethnio

[Ethnio](https://ethn.io) helps you find the right participants for your
in-person and remote UX research, online exercises or surveys, to create
beautiful recruiting screeners.

## Description

Ethnio is similar to a survey in functionality but built exclusively for
researchers. The basic functionality is that you can create a web-based
form with custom questions, place JavaScript on your website and then control 
the display of that form from inside of ethnio. We call these things screeners.
A visitor to your website that views an ethnio form can submit answers that
go directly to Ethnio servers, either via SSL or regular HTTP

## Requirements

Ethnio account.

## Installation

* Follow the standard [Drupal module installation](https://drupal.org/documentation/install/modules-themes)
  process

## Configuration

Visit `admin/config/system/ethnio` and fill in the Ethnio ID field. Your 
Ethnio ID can be found in your code `src=\"//ethn.io/12345.js` where
**12345** is your Ethnio ID. Give the `administer ethnio` permission
to configure Ethnio module.

### Page specific settings

The default is set to "Add to every page except the listed pages". By
default the following pages are listed for exclusion: (more details about
this pages [here](http://drupal.org/node/34970))

admin
admin/*
batch
node/add*
node/*/*
user/*/*

These defaults are changeable by the website administrator or any other
user with *Administer Ethnio* permission.

### User specific settings

The default is set to appear for every user role on the site. But can be
changed by the website administrator or any other user with 
*Administer Ethnio* permission.

### Ethnio display probability

This controls what fraction of site visitors will see the Ethnio popup. This
probability is calculated using `Math.random()`. For example, if 100% is selected
this means that the ethnio script will appear on the page for every user (that
meets the user role and page requirements stated above.) If 10% is selected
then only the probability is that 1 user in 10 will see the popup.

*NOTE:* Ethnio sets a COOKIE `ethnio_displayed` when it is shown to a user so
this user won't see the same survey more than once. This means that for debug
purposes is advised to use Incognito/Private tabs.